let displayValue = '0';
let firstOperand = null;
let waitingForSecondOperand = false;
let operator = null;

function updateDisplay() {
    const display = document.querySelector('#box');
    display.innerText = displayValue;
}

function button_number(value) {
    if (waitingForSecondOperand === true) {
        displayValue = value;
        waitingForSecondOperand = false;
    } else {
        displayValue = displayValue === '0' ? value : displayValue + value;
    }

    updateDisplay();
}

function button_clear() {
    displayValue = '0';
    firstOperand = null;
    waitingForSecondOperand = false;
    operator = null;
    updateDisplay();
}

function clear_entry() {
    displayValue = '0';
    updateDisplay();
}

function backspace_remove() {
    displayValue = displayValue.slice(0, -1);
    if (displayValue === '') {
        displayValue = '0';
    }
    updateDisplay();
}

function plus_minus() {
    displayValue = (parseFloat(displayValue) * -1).toString();
    updateDisplay();
}

function calculate_percentage() {
    displayValue = (parseFloat(displayValue) / 100).toString();
    updateDisplay();
}

function division_one() {
    displayValue = (1 / parseFloat(displayValue)).toString();
    updateDisplay();
}

function power_of() {
    displayValue = (parseFloat(displayValue) ** 2).toString();
    updateDisplay();
}

function square_root() {
    displayValue = Math.sqrt(parseFloat(displayValue)).toString();
    updateDisplay();
}

function handleOperator(nextOperator) {
    const inputValue = parseFloat(displayValue);

    if (operator && waitingForSecondOperand) {
        operator = nextOperator;
        return;
    }

    if (firstOperand == null) {
        firstOperand = inputValue;
    } else if (operator) {
        const result = calculate(firstOperand, inputValue, operator);

        displayValue = `${parseFloat(result.toFixed(7))}`;
        firstOperand = result;
    }

    waitingForSecondOperand = true;
    operator = nextOperator;

    updateDisplay();
}

function calculate(firstOperand, secondOperand, operator) {
    if (operator === '+') {
        return firstOperand + secondOperand;
    } else if (operator === '-') {
        return firstOperand - secondOperand;
    } else if (operator === '*') {
        return firstOperand * secondOperand;
    } else if (operator === '/') {
        return firstOperand / secondOperand;
    }

    return secondOperand;
}

document.querySelectorAll('.operator').forEach(button => {
    button.addEventListener('click', event => {
        handleOperator(event.target.value);
    });
});

document.querySelectorAll('button').forEach(button => {
    if (!button.classList.contains('operator')) {
        button.addEventListener('click', event => {
            button_number(event.target.innerText);
        });
    }
});

document.addEventListener('DOMContentLoaded', updateDisplay);
